This is a simple script that installs and starts docker on CentOS 6.6.
Once the script has been run, you can download docker images using:
	"sudo docker pull $image" 
and you can view the images stored locally using: 
	"sudo docker images"


